﻿using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer
{
    public static class SeedData
    {
        public static void EnsurePopulated(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityRole<int>>().HasData(
                 new IdentityRole<int> { Id = 1, Name = "Admin", NormalizedName = "ADMIN" },
                 new IdentityRole<int> { Id = 2, Name = "Teacher", NormalizedName = "TEACHER" },
                 new IdentityRole<int> { Id = 3, Name = "Student", NormalizedName = "STUDENT" }
             );

            modelBuilder.Entity<AppUser>().HasData(
                new AppUser
                {
                    Id = 1,
                    Email = "classtest.sup@gmail.com",
                    NormalizedEmail = "CLASSTEST.SUP@GMAIL.COM",
                    UserName = "classtest.sup@gmail.com",
                    NormalizedUserName = "CLASSTEST.SUP@GMAIL.COM",
                    PasswordHash = "AQAAAAEAACcQAAAAEB6nhSmfafKSOAv5UAV/JISBv6pa60PF60a3Izt+qc+mqpIvj4TtRU5AUj5an4eNiQ==",
                    SecurityStamp = "SMAYDCXD3PQC3QNONXGNFXZGS4VTEO4H",
                    ConcurrencyStamp = "13eb48ba-ccc3-4a11-baca-7c92ccaf72fe",
                    LockoutEnabled = true
                },
                new AppUser
                {
                    Id = 2,
                    Email = "vovapetrov7@ukr.net",
                    NormalizedEmail = "VOVAPETROV7@UKR.NET",
                    UserName = "vovapetrov7@ukr.net",
                    NormalizedUserName = "VOVAPETROV7@UKR.NET",
                    PasswordHash = "AQAAAAEAACcQAAAAEFi5dZuLEuj9ZEIWP46Q270Lm7ay5M/I5NfbTq+2p3afBli5T9H3GC09QmMOGAnMQQ==",
                    SecurityStamp = "TKUETEYI3SNLGSKO3XZVIDNLNYSNKDZY",
                    ConcurrencyStamp = "813ea4ae-507b-4b60-8c45-bea9e3c3924e",
                    LockoutEnabled = true
                },
                new AppUser
                {
                    Id = 3,
                    Email = "vova.petrov0708@gmail.com",
                    NormalizedEmail = "VOVA.PETROV0708@GMAIL.COM",
                    UserName = "vova.petrov0708@gmail.com",
                    NormalizedUserName = "VOVA.PETROV0708@GMAIL.COM",
                    PasswordHash = "AQAAAAEAACcQAAAAEDv87w8ee3ylA7hMLIptnHFjJ1eBBQjjPP49wsTmt9pZIjnh6+LTuoSh0CcXRE8+EQ==",
                    SecurityStamp = "LGJTU7GDHDM47COFQAUTVQZCC7MFFJBL",
                    ConcurrencyStamp = "fd56ad91-e0fd-468c-8c30-7451b2d0ec5a",
                    LockoutEnabled = true
                }
           );

            modelBuilder.Entity<IdentityUserRole<int>>().HasData(
               new IdentityUserRole<int> { UserId = 1, RoleId = 1 },
               new IdentityUserRole<int> { UserId = 2, RoleId = 2 },
               new IdentityUserRole<int> { UserId = 3, RoleId = 3 }
               );

            modelBuilder.Entity<Teacher>().HasData(new Teacher { Id = 1, AppUserId = 2 });

            modelBuilder.Entity<Specialty>().HasData(new Specialty { Id = 1, Name = "Комп'ютерна інженерія", PartialName = "OK", YearsOfStuding = 4 });

            modelBuilder.Entity<Group>().HasData(new Group { Id = 1, UUID = Guid.NewGuid().ToString(), SpecialtyId = 1, YearOfEntry = 2016, Number = 1 });

            modelBuilder.Entity<Student>().HasData(new Student { Id = 1, AppUserId = 3, GroupId = 1 });

        }
    }
}
