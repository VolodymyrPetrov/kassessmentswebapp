﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Services
{
    public class ContentCreater
    {
        public string GetRegisterEmailContent(Guid UUID,AppUser user)
        {
            string url = "https://localhost:44337/Account/FinishRegistring/"+UUID;
            return $"Шановний {user.FirstName}, для вас було створено обліковий запис в системі" +
                $"тестування знань KAssessment. Для завершення реєстрації перейдіть за посиланням нижче:" +
                $"<a href='{url}'>Завершити реєстрацію</a>";
        }
    }
}
