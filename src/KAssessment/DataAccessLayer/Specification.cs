﻿using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
namespace DataAccessLayer
{
    public class Specification
    {
        private readonly DataContext dataContext;
        public int CurrentUserId { get; set; }
        private readonly IHttpContextAccessor accessor;
        public Specification(DataContext dataContext, IHttpContextAccessor accessor)
        {
            this.accessor = accessor;
            this.dataContext = dataContext;

            if (!string.IsNullOrEmpty(this.accessor.HttpContext?.User?.Identity?.Name))
                Configure(accessor.HttpContext.User.Identity.Name);
        }
        public void Configure(string userName)
        {
            var user = GetUserByUsername(userName);
            CurrentUserId = user.Id;
        }
        public int GetUserId()
        {
            return CurrentUserId;
        }
        public IEnumerable<int> GetStudentGroupIds(int studentId = 0)
        {
            studentId = studentId != 0 ? studentId : GetUserId();
            var student = dataContext.Students.FirstOrDefault(e => e.Id == studentId);
            if (student != null)
                return new List<int> { student.GroupId };
            else 
                return new List<int> { };
        }
        public AppUser GetUserByUsername(string username)
        {
            return dataContext.Users.FirstOrDefault(e => e.UserName == username);
        }
    }
}
