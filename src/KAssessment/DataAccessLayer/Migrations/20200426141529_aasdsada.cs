﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class aasdsada : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GroupSession_Courses_CourseId",
                table: "GroupSession");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupSession_Tests_TestId",
                table: "GroupSession");

            migrationBuilder.DropForeignKey(
                name: "FK_Sessions_GroupSession_GroupSessionId",
                table: "Sessions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GroupSession",
                table: "GroupSession");

            migrationBuilder.RenameTable(
                name: "GroupSession",
                newName: "GroupSessions");

            migrationBuilder.RenameIndex(
                name: "IX_GroupSession_TestId",
                table: "GroupSessions",
                newName: "IX_GroupSessions_TestId");

            migrationBuilder.RenameIndex(
                name: "IX_GroupSession_CourseId",
                table: "GroupSessions",
                newName: "IX_GroupSessions_CourseId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GroupSessions",
                table: "GroupSessions",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "e86bfb6c-7654-4f14-ab4d-8948bd42c6ff");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "3348cbdb-1d5b-4850-9555-e259a359231d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "aa5b9778-8ed2-4e4d-95b0-5e6652451c19");

            migrationBuilder.UpdateData(
                table: "Groups",
                keyColumn: "Id",
                keyValue: 1,
                column: "UUID",
                value: "a4d9c590-0a7f-419c-808b-cf6fab8c8a31");

            migrationBuilder.AddForeignKey(
                name: "FK_GroupSessions_Courses_CourseId",
                table: "GroupSessions",
                column: "CourseId",
                principalTable: "Courses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupSessions_Tests_TestId",
                table: "GroupSessions",
                column: "TestId",
                principalTable: "Tests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sessions_GroupSessions_GroupSessionId",
                table: "Sessions",
                column: "GroupSessionId",
                principalTable: "GroupSessions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GroupSessions_Courses_CourseId",
                table: "GroupSessions");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupSessions_Tests_TestId",
                table: "GroupSessions");

            migrationBuilder.DropForeignKey(
                name: "FK_Sessions_GroupSessions_GroupSessionId",
                table: "Sessions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GroupSessions",
                table: "GroupSessions");

            migrationBuilder.RenameTable(
                name: "GroupSessions",
                newName: "GroupSession");

            migrationBuilder.RenameIndex(
                name: "IX_GroupSessions_TestId",
                table: "GroupSession",
                newName: "IX_GroupSession_TestId");

            migrationBuilder.RenameIndex(
                name: "IX_GroupSessions_CourseId",
                table: "GroupSession",
                newName: "IX_GroupSession_CourseId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GroupSession",
                table: "GroupSession",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "51beeaca-862b-4470-9005-8b46cf53e1e2");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "2114e196-d621-44ea-af81-3c9082fe1ab9");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "9d09e848-8583-4c2f-aa17-086fd69f3812");

            migrationBuilder.UpdateData(
                table: "Groups",
                keyColumn: "Id",
                keyValue: 1,
                column: "UUID",
                value: "d278e227-2954-4c9f-b9d8-d76be85ef074");

            migrationBuilder.AddForeignKey(
                name: "FK_GroupSession_Courses_CourseId",
                table: "GroupSession",
                column: "CourseId",
                principalTable: "Courses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupSession_Tests_TestId",
                table: "GroupSession",
                column: "TestId",
                principalTable: "Tests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sessions_GroupSession_GroupSessionId",
                table: "Sessions",
                column: "GroupSessionId",
                principalTable: "GroupSession",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
