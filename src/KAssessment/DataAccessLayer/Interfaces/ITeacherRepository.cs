﻿using Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface ITeacherRepository
    {
        Task ModifyTeacher(TeacherInfoModel teacherInfo);
        TeacherInfoModel GetTeacher(int teacherId);
        TeacherInfoModel GetTeacherByUserName(string UserName);
        IEnumerable<TeacherInfoModel> GetTeachers();
        void DeactivateTeacher(int teacherId);
        void ActivateTeacher(int teacherId);
        IEnumerable<Course> GetTeacherCourses(string userName);
        IEnumerable<TestsListItemDTO> GetTeacherTests(string userName,int? subjectId=null);
        void CreateCourse(CreateCourseDTO model);
    }
}
