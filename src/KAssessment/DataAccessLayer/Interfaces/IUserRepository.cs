﻿using Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IUserRepository
    {
        Task FinishRegistrigFromEmailLink(Guid uuid, string password);
        FinishRegistringFromLinkModel GetModelForFinishingRegistrigFromEmailLink(Guid uuid);
    }
}
