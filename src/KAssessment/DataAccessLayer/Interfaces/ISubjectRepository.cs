﻿using Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Interfaces
{
    public interface ISubjectRepository
    {
        void ModifySubject(Subject subject);
        Subject GetSubject(int Id);
        IEnumerable<Subject> GetSubjects(int[] GroupIds, int[] SubjectIds, int[] TeacherIds);
        IEnumerable<Subject> GetSubjects();
    }
}
