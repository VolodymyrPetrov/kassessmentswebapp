﻿using Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Interfaces
{
    public interface ITestRepository
    {
        int ModifyTest(Test test);
        Test GetTest(int id);
        IEnumerable<Test> GetTests();
        Test GetTestWithQuestions(int id);
        int ModifyTestQuestion(Question question);
        void DeleteTest(int testId);
        Answer ModifyQuestionAnswer(Answer answer);
        Question GetQuestion(int id);
        Answer GetAnswer(int id);
        void DeleteAnswer(int id);
        void ModifySession(TestSession testSession);
        IEnumerable<TestSession> GetSessions(int courseId);
        IEnumerable<SessionTestItem> GetActiveTestForStudent(int studentId);
    }
}
