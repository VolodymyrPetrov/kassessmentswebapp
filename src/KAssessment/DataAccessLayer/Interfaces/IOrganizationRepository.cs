﻿using Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IOrganizationRepository
    {
        Task<int> EditSpecialty(Specialty specialty);
        void DeleteSpecialty(int specialtyId);
        Task<Specialty> GetSpecialty(int specialtyId);
        IEnumerable<Specialty> GetSpecialties();
        void EditGroup(Group group);
        void DeleteGroup(int groupId);
        Group GetGroup(int groupId);
        IEnumerable<Group> GetGroups(int SpecialtyId = 0);
        IEnumerable<Group> GetAvailableGroups();
        StudentGroupDTO GetGroupStudents(int groupId);
    }
}
