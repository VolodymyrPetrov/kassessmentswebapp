﻿using Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Interfaces
{
    public interface IStudentRepository
    {
        void RegisterStudent(int UserId, string GroupCode);
        bool CheckGroup(string GroupCode);
        StudentInfoModel GetStudentByUsername(string username);
    }
}
