﻿using Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Interfaces
{
    public interface ICourseRepository
    {
        Course GetCourseInfo(int subjectId, int groupId, int teacherId);
    }
}
