﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Entities
{
    public class Test
    {
        public int Id { get; set; }
        public int TeacherId { get; set; }
        public virtual Teacher Teacher { get; set; }
        public string Description { get; set; }
        public int SubjectId { get; set; }
        public virtual Subject Subject { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Question> Questions { get; set; } = new List<Question>();
        public virtual ICollection<GroupSession> GroupSessions { get; set; } = new List<GroupSession>();
        public int? StandartNumberOfQuestions { get; set; }
    }
}
