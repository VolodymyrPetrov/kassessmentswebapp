﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Entities
{
    public class Answer
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string MediaResource { get; set; }
        public bool IsRight { get; set; }
        public virtual Question Question { get; set; }
        public int QuestionId { get; set; }
    }
}
