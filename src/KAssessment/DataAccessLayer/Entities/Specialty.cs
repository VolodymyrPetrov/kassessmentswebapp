﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataAccessLayer.Entities
{
    public class Specialty
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PartialName { get; set; }
        public byte YearsOfStuding { get; set; }
    }
}
