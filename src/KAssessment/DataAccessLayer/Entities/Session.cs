﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataAccessLayer.Entities
{
    public class Session
    {
        public int Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? DateToLive { get; set; }
        public virtual Student Student { get; set; }
        public int StudentId { get; set; }
        public byte? Mark { get; set; }
        public virtual GroupSession GroupSession { get; set; }
        public int? GroupSessionId { get; set; }
    }
}
