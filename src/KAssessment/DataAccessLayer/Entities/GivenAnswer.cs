﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Entities
{
    public class GivenAnswer
    {
        public int Id { get; set; }
        public virtual Session Session { get; set; }
        public virtual Answer Answer { get; set; }
        public int AnswerId { get; set; }
    }
}
