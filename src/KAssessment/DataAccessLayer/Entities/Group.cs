﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Entities
{
    public class Group
    {
        public int Id { get; set; }
        public int SpecialtyId { get; set; }
        public virtual Specialty Specialty { get; set; }
        public short YearOfEntry { get; set; }
        public byte Number { get; set; }
        public virtual ICollection<Student> Students { get; set; } = new List<Student>();
        public string UUID { get; set; }
        public virtual ICollection<Course> Courses { get; set; } = new List<Course>();
    }
}
