﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataAccessLayer.Entities
{
    public class ConfirmRegister
    {
        [Key]
        public Guid UUID { get; set; }
        public int UserId { get; set; }
    }
}
