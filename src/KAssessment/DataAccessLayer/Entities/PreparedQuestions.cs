﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Entities
{
    public class PreparedQuestions
    {
        public int Id { get; set; }
        public int SessionId { get; set; }
        public virtual Session Session { get; set; }
        public string QuestionsIds { get; set; }
    }
}
