﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Extensions
{
    public static class AppUserExtensions
    {
        public static string GetPersonName(this AppUser user)
        {
            string personName = user.LastName;
            if (!string.IsNullOrEmpty(user.FirstName))
            {
                personName += " " + user.FirstName;
            }
            if (!string.IsNullOrEmpty(user.MiddleName))
            {
                personName += " " + user.MiddleName;
            }
            return personName??"";
        }
    }
}
