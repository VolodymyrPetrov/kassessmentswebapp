﻿using System;
using System.Collections.Generic;
using System.Text;
using Db = DataAccessLayer.Entities;
using WebModel = Models.Entities;
using DataAccessLayer.Extensions;
using System.Linq;

namespace DataAccessLayer
{
    public class Converter
    {
        private readonly DataContext dataContext;
        public Converter(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }
        public   WebModel.Group ConvertToGroup(Db.Group dbGroup)
        {
            return new WebModel.Group
            {
                Id = dbGroup.Id,
                SpecialtyId = dbGroup.SpecialtyId,
                YearOfEntry = dbGroup.YearOfEntry,
                Name = getGroupName(dbGroup),
                Number = dbGroup.Number,
                YearOfEnd = (short)(dbGroup.YearOfEntry + dbGroup?.Specialty?.YearsOfStuding),
                Specialty = ConvertSpecialtyToModel(dbGroup.Specialty),
                UUID = dbGroup.UUID
            };
        }
        public   WebModel.StudentInfoModel ConvertToStudentInfo(Db.Student dbStudent)
        {
            return new WebModel.StudentInfoModel
            {
                StudentId = dbStudent.Id,
                StudentName = dbStudent.AppUser.FirstName + " " + dbStudent.AppUser.LastName
            };
        }
        public   WebModel.TestSession ConvertToSession(Db.GroupSession session)
        {
            return new WebModel.TestSession
            {
                CourseId = session.CourseId,
                CreateDate = session.CreatedDate,
                EndDate = session.EndDate,
                TestId = session.TestId.Value,
                Test = ConvertToTestTiny(session.Test)
            };
        }
        public   WebModel.Answer ConvertToAnswer(Db.Answer answer)
        {
            return new WebModel.Answer
            {
                Id = answer.Id,
                IsRight = answer.IsRight,
                QuestionId = answer.QuestionId,
                Text = answer.Text
            };
        }
        public   WebModel.Test ConvertToTestWithQuestions(Db.Test dbTest)
        {
            var test = ConvertToTest(dbTest);
            test.Questions = dbTest.Questions.Select(e => ConvertToQuestion(e)).OrderByDescending(e => e.Id);
            return test;
        }
        public   WebModel.Question ConvertToQuestion(Db.Question question)
        {
            var model = ConvertToQuestionTiny(question);
            model.Answers = question.Answers.Select(a => ConvertToAnswer(a));
            return model;
        }
        public   WebModel.Question ConvertToQuestionTiny(Db.Question question)
        {
            return new WebModel.Question
            {
                Id = question.Id,
                QuestionText = question.Text
            };
        }
        public   WebModel.Test ConvertToTestTiny(Db.Test test)
        {
            return new WebModel.Test
            {
                Id = test.Id,
                Name = test.Name,
                SubjectId = test.SubjectId,
                TeacherId = test.TeacherId
            };
        }
        public   WebModel.SessionTestItem ConvertToTestSessionItem(Db.Session session)
        {
            string subjectName = dataContext.GroupSessions
                .FirstOrDefault(e => e.Id == session.GroupSessionId)
                .Course.Subject.Name;

            var test = dataContext.GroupSessions.FirstOrDefault(e => e.Id == session.GroupSessionId).Test;
            return new WebModel.SessionTestItem
            {
                StudentId = session.StudentId,
                PersonSessionId = session.Id,
                Mark = session.Mark,
                GroupSessionId = session.GroupSessionId,
                SubjectName = subjectName,
                Test = new WebModel.Test { Id=test.Id,Name=test.Name },
                StartDate = session.StartDate,
                EndDate =session.EndDate
            };
        }
        public   WebModel.Test ConvertToTest(Db.Test test)
        {
            return new WebModel.Test
            {
                Id = test.Id,
                Name = test.Name,
                Description = test.Description,
                NumberOfQuestions = test.StandartNumberOfQuestions,
                SubjectId = test.SubjectId,
                TeacherId = test.TeacherId
            };
        }
        public   WebModel.Course ConvertToCourse(Db.Course course)
        {
            return new WebModel.Course
            {
                Id = course.Id,
                Group = ConvertToGroup(course.Group),
                Subject = ConvertSubjectToModel(course.Subject),
                Teacher = ConvertTeacherToModel(course.Teacher),
                CreatedDate = course.CreatedDate,
                TestSessions = course.GroupSessions.OrderByDescending(e => e.CreatedDate).Select(e => ConvertToSession(e))
            };
        }
        public   WebModel.StudentGroupDTO ConvertGroupStudents(IEnumerable<Db.Student> students, WebModel.Group group)
        {
            return new WebModel.StudentGroupDTO
            {
                GroupName = group.Name,
                Students = students.Select(e => new WebModel.StudentInfoModel { StudentId = e.Id, StudentName = e.AppUser.FirstName + " " + e.AppUser.LastName })
            };
        }
        public   WebModel.TeacherInfoModel ConvertTeacherToModel(Db.Teacher teacher)
        {
            var model = new WebModel.TeacherInfoModel
            {
                Id = teacher.Id,
                Email = teacher.AppUser?.Email,
                FirstName = teacher.AppUser?.FirstName,
                LastName = teacher.AppUser?.LastName,
                MiddleName = teacher.AppUser?.MiddleName,
                FullName = teacher.AppUser?.GetPersonName(),
                IsDeleted = (bool)teacher.AppUser?.IsDeleted
            };
            return model;
        }
        public   WebModel.Specialty ConvertSpecialtyToModel(Db.Specialty dbSpecialty)
        {
            var specialty = new WebModel.Specialty
            {
                Id = dbSpecialty.Id,
                Name = dbSpecialty.Name,
                PartialName = dbSpecialty.PartialName,
                YearsOfStuding = dbSpecialty.YearsOfStuding
            };
            return specialty;
        }
        public   WebModel.Subject ConvertSubjectToModel(Db.Subject dbSubject)
        {
            WebModel.Subject subject = new WebModel.Subject
            {
                Id = dbSubject.Id,
                Name = dbSubject.Name
            };
            return subject;
        }
        private   string getGroupName(Db.Group group)
        {
            DateTime currentDate = DateTime.Now;
            int currentYear = currentDate.Year - group.YearOfEntry;
            if (currentDate.Month >= 9)
            {
                currentYear++;
            }
            if (currentYear > group.Specialty.YearsOfStuding)
                currentYear = group.Specialty.YearsOfStuding;

            return currentYear + group.Specialty.PartialName + group.Number;
        }
    }
}
