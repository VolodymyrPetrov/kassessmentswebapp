﻿using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer
{
    public class Repository
    {
        public Repository(
            ITeacherRepository teacherRepository, 
            IOrganizationRepository organizationRepository,
            ISubjectRepository subjectRepository,
            IStudentRepository studentRepository,
            IUserRepository userRepository,
            ICourseRepository courseRepository,
            ITestRepository testRepository)
        {
            Teachers = teacherRepository;
            Organization = organizationRepository;
            Subjects = subjectRepository;
            Students = studentRepository;
            Users = userRepository;
            Course = courseRepository;
            Tests = testRepository;
        }
        public ITeacherRepository Teachers { get; set; }
        public IOrganizationRepository Organization { get; set; }
        public ISubjectRepository Subjects { get; set; }
        public IStudentRepository Students { get; set; }
        public IUserRepository Users { get; set; }
        public ITestRepository Tests { get; set; }
        public ICourseRepository Course { get; set; }
    }
}
