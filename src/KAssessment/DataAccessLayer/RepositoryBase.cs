﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer
{
    public class RepositoryBase
    {
        protected readonly DataContext dataContext;
        protected readonly Specification Specification;
        protected readonly Converter Converter;
        public RepositoryBase(DataContext dataContext, Specification Specification, Converter converter)
        {
            this.dataContext = dataContext;
            this.Specification = Specification;
            this.Converter = converter;
        }
    }
}
