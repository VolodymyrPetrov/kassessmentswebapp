﻿using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using Microsoft.AspNetCore.Identity;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext dataContext;
        private readonly UserManager<AppUser> userManager;
        public UserRepository(DataContext dataContext, UserManager<AppUser> userManager)
        {
            this.dataContext = dataContext;
            this.userManager = userManager;
        }
        public async Task FinishRegistrigFromEmailLink(Guid uuid,string password)
        {
            var registrationConfirm = dataContext.ConfirmRegisters.FirstOrDefault(e => e.UUID == uuid);
            var userId = registrationConfirm?.UserId;
            var user = dataContext.Users.FirstOrDefault(e => e.Id == userId);
            if(user!=null)
            {
                await userManager.AddPasswordAsync(user, password);
                dataContext.ConfirmRegisters.Remove(registrationConfirm);
                dataContext.SaveChanges();
            }
        }

        public FinishRegistringFromLinkModel GetModelForFinishingRegistrigFromEmailLink(Guid uuid)
        {
            var registrationConfirm = dataContext.ConfirmRegisters.FirstOrDefault(e => e.UUID == uuid);
            var userId = registrationConfirm?.UserId;
            var user = dataContext.Users.FirstOrDefault(e => e.Id == userId);
            if (user != null)
            {
                FinishRegistringFromLinkModel model = new FinishRegistringFromLinkModel
                {
                    uuid = uuid,
                    Email = user.Email
                };
                return model;
            }
            else
            {
                throw new Exception("Something went wrong");
            }
        }
    }
}
