﻿using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebModel = Models.Entities;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class OrganizationRepository : IOrganizationRepository
    {
        private readonly Converter Converter;
        private readonly DataContext dataContext;
        public OrganizationRepository(DataContext dataContext, Converter converter)
        {
            this.dataContext = dataContext;
            this.Converter = converter;
        }
        public void DeleteGroup(int groupId)
        {
            var groupDb = groupId != 0 ? dataContext.Groups.FirstOrDefault(e => e.Id == groupId) : null;
            if (groupDb != null)
            {
                dataContext.Groups.Remove(groupDb);
                dataContext.SaveChanges();
            }
        }

        public void DeleteSpecialty(int specialtyId)
        {
            var specialtyDb = specialtyId != 0 ? dataContext.Specialties.FirstOrDefault(e => e.Id == specialtyId) : null;
            if (specialtyDb != null)
            {
                dataContext.Specialties.Remove(specialtyDb);
                dataContext.SaveChanges();
            }
        }

        public void EditGroup(WebModel.Group group)
        {
            var groupDb = group.Id != 0 ? dataContext.Groups.FirstOrDefault(e => e.Id == group.Id) : null;
            if (groupDb == null)
            {
                groupDb = new Group()
                {
                    SpecialtyId = group.SpecialtyId,
                    YearOfEntry = group.YearOfEntry,
                    Number = group.Number,
                    UUID = generateGroupUUID()
                };
                dataContext.Groups.Add(groupDb);
            }
            else
            {
                groupDb.SpecialtyId = group.SpecialtyId;
                groupDb.YearOfEntry = group.YearOfEntry;
                groupDb.Number = group.Number;
            }
            dataContext.SaveChanges();
        }

        public async Task<int> EditSpecialty(WebModel.Specialty specialty)
        {
            var specialtyDb = specialty.Id != 0 ? dataContext.Specialties.FirstOrDefault(e => e.Id == specialty.Id) : null;
            if (specialtyDb == null)
            {
                specialtyDb = new Specialty()
                {
                    Name = specialty.Name,
                    PartialName = specialty.PartialName,
                    YearsOfStuding = specialty.YearsOfStuding
                };
                await dataContext.Specialties.AddAsync(specialtyDb);
            }
            else
            {
                specialtyDb.Name = specialty.Name;
                specialtyDb.PartialName = specialty.PartialName;
                specialtyDb.YearsOfStuding = specialty.YearsOfStuding;
            }
            await dataContext.SaveChangesAsync();
            return specialtyDb.Id;
        }

        public IEnumerable<WebModel.Group> GetAvailableGroups()
        {
            var dateNow = DateTime.Now;
            int year = dateNow.Month >= 8 && dateNow.Month <= 12 ? dateNow.Year + 1 : dateNow.Year;
            var groups = dataContext.Groups.Include(e=>e.Specialty).Where(e => (e.YearOfEntry + e.Specialty.YearsOfStuding) >= year);
            return groups.Select(e => Converter.ConvertToGroup(e));
        }

        public WebModel.Group GetGroup(int groupId)
        {
            var dbGroup = dataContext.Groups.Include(e => e.Specialty).FirstOrDefault(e => e.Id == groupId);
            return Converter.ConvertToGroup(dbGroup);
        }

        public IEnumerable<WebModel.Group> GetGroups(int SpecialtyId = 0)
        {
            IEnumerable<Group> dbGroups;
            if (SpecialtyId != 0)
            {
                dbGroups = dataContext.Groups.Include(e => e.Specialty).Where(e => e.SpecialtyId == SpecialtyId).ToList();
            }
            else
            {
                dbGroups = dataContext.Groups.Include(e => e.Specialty).ToList();
            }
            var groups = dbGroups.Select(e => Converter.ConvertToGroup(e));
            return groups.OrderByDescending(e => e.YearOfEnd);
        }

        public WebModel.StudentGroupDTO GetGroupStudents(int groupId)
        {
            var dbStudentGroups = dataContext.Students.Include(e=>e.AppUser).Where(e => e.GroupId == groupId);
            return Converter.ConvertGroupStudents(dbStudentGroups,GetGroup(groupId));
        }

        public IEnumerable<WebModel.Specialty> GetSpecialties()
        {
            return dataContext.Specialties.AsQueryable().Select(e => Converter.ConvertSpecialtyToModel(e));
        }

        public async Task<WebModel.Specialty> GetSpecialty(int specialtyId)
        {
            var dbSpecialty = await dataContext.Specialties.FirstOrDefaultAsync(e => e.Id == specialtyId);
            if (dbSpecialty == null)
            {
                return new WebModel.Specialty { };
            }
            return Converter.ConvertSpecialtyToModel(dbSpecialty);
        }
        private string generateGroupUUID()
        {
            var guid = Guid.NewGuid().ToString().Substring(0, 8);
            var itemWithSameGuid = dataContext.Groups.FirstOrDefault(e => e.UUID == guid);
            if (itemWithSameGuid == null)
            {
                return guid;
            }
            return generateGroupUUID();
        }
    }
}
