﻿using DataAccessLayer.Interfaces;
using WebModels = Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using DataAccessLayer.Entities;

namespace DataAccessLayer.Repositories
{
    public class SubjectRepository:ISubjectRepository
    {
        private readonly DataContext dataContext;
        private readonly Converter Converter;
        public SubjectRepository(DataContext dataContext,Converter converter)
        {
            this.dataContext = dataContext;
            this.Converter = converter;
        }
        public void ModifySubject(WebModels.Subject subject)
        {
            Subject dbSubject;
            if (subject.Id == 0)
            {
                dbSubject = new Subject
                {
                    Name = subject.Name
                };
                dataContext.Subjects.Add(dbSubject);
            }
            else
            {
                dbSubject = getDbSubject(subject.Id);
                if(dbSubject!=null)
                {
                    dbSubject.Name = subject.Name;
                }
            }
            dataContext.SaveChanges();
        }
        public Subject getDbSubject(int id)
        {
            return dataContext.Subjects.FirstOrDefault(e => e.Id == id);
        }
        public WebModels.Subject GetSubject(int Id)
        {
            var dbSubject = getDbSubject(Id);
            if (dbSubject == null)
                throw new Exception("Subject not found");

            return Converter.ConvertSubjectToModel(dbSubject);
        }
        public IEnumerable<WebModels.Subject> GetSubjects()
        {
            return dataContext.Subjects.Select(e=> Converter.ConvertSubjectToModel(e));
        }
        public IEnumerable<WebModels.Subject> GetSubjects(int[] GroupIds,int[] SubjectIds,int[] TeacherIds)
        {
            var dbSubjects = dataContext.Courses.AsQueryable();

            if (GroupIds!=null)
            {
                dbSubjects = dbSubjects.Where(e => GroupIds.Contains(e.GroupId));
            }
            if (SubjectIds != null)
            {
                dbSubjects = dbSubjects.Where(e => SubjectIds.Contains(e.SubjectId));
            }
            if (TeacherIds != null)
            {
                dbSubjects = dbSubjects.Where(e => TeacherIds.Contains(e.TeacherId));
            }
            return dbSubjects.Select(e=> Converter.ConvertSubjectToModel(e.Subject));
        }
        public void DeleteSubject(int SubjectId)
        {
            var dbSubject = dataContext.Subjects.FirstOrDefault(e=>e.Id==SubjectId);
            dataContext.Subjects.Remove(dbSubject);
            dataContext.SaveChanges();
        }
    }
}
