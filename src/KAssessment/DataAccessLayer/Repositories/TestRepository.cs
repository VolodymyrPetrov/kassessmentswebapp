﻿using BusinessLogic.Exceptions;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using Microsoft.EntityFrameworkCore;
using Models;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogic.Extensions;
using WebModels = Models.Entities;
namespace DataAccessLayer.Repositories
{
    public class TestRepository : RepositoryBase, ITestRepository
    {
        public TestRepository(DataContext dataContext,
            Specification Specification,
            Converter converter) : base(dataContext, Specification, converter)
        {

        }

        public int ModifyTest(WebModels.Test test)
        {
            Entities.Test dbTest;
            if (test.Id == 0)
            {
                dbTest = new Entities.Test
                {
                    TeacherId = test.TeacherId,
                    Name = test.Name,
                    Description = test.Description,
                    StandartNumberOfQuestions = test.NumberOfQuestions,
                    SubjectId = test.SubjectId
                };
                dataContext.Tests.Add(dbTest);
            }
            else
            {
                dbTest = dataContext.Tests.FirstOrDefault(e => e.Id == test.Id);
                dbTest.Name = test.Name;
                dbTest.Description = test.Description;
                dbTest.SubjectId = test.SubjectId;
                dbTest.StandartNumberOfQuestions = test.NumberOfQuestions;
            }
            dataContext.SaveChanges();
            return dbTest.Id;
        }

        public WebModels.Test GetTest(int id)
        {
            var test = dataContext.Tests.FirstOrDefault(e => e.Id == id);
            if (test == null)
                throw new Exception("Test not found");
            return Converter.ConvertToTest(test);
        }

        public IEnumerable<WebModels.Test> GetTests()
        {
            throw new NotImplementedException();
        }

        public WebModels.Test GetTestWithQuestions(int id)
        {
            var test = dataContext.Tests.Include(e => e.Questions).FirstOrDefault(e => e.Id == id);
            foreach (var q in test.Questions)
            {
                q.Answers = dataContext.Answers.Where(e => e.QuestionId == q.Id).ToList();
            }

            if (test == null)
                throw new Exception("Test not found");
            return Converter.ConvertToTestWithQuestions(test);
        }

        public int ModifyTestQuestion(WebModels.Question question)
        {
            Entities.Question dbQuestion;
            if (question.Id == 0)
            {
                dbQuestion = new Entities.Question
                {
                    TestId = question.TestId,
                    Text = question.QuestionText
                };
                dataContext.Questions.Add(dbQuestion);
            }
            else
            {
                dbQuestion = dataContext.Questions.FirstOrDefault(e => e.Id == question.Id);
                dbQuestion.Text = question.QuestionText;
            }
            dataContext.SaveChanges();
            return dbQuestion.Id;
        }

        public void DeleteTest(int testId)
        {
            var test = dataContext.Tests.FirstOrDefault(e => e.Id == testId);
            dataContext.Tests.Remove(test);
            dataContext.SaveChanges();
        }

        public WebModels.Answer ModifyQuestionAnswer(WebModels.Answer answer)
        {
            Entities.Answer dbAnswer;
            if (answer.Id == 0)
            {
                dbAnswer = new Entities.Answer
                {
                    IsRight = answer.IsRight,
                    QuestionId = answer.QuestionId,
                    Text = answer.Text
                };
                dataContext.Answers.Add(dbAnswer);
            }
            else
            {
                dbAnswer = dataContext.Answers.FirstOrDefault(e => e.Id == answer.Id);
                dbAnswer.IsRight = answer.IsRight;
                dbAnswer.Text = answer.Text;
            }
            dataContext.SaveChanges();
            return Converter.ConvertToAnswer(dbAnswer);
        }

        public WebModels.Question GetQuestion(int id)
        {
            var question = dataContext.Questions.Include(e => e.Answers).FirstOrDefault(e => e.Id == id);
            return Converter.ConvertToQuestion(question);
        }

        public WebModels.Answer GetAnswer(int id)
        {
            var answer = dataContext.Answers.FirstOrDefault(e => e.Id == id);
            return Converter.ConvertToAnswer(answer);
        }

        public void DeleteAnswer(int id)
        {
            var answer = dataContext.Answers.FirstOrDefault(e => e.Id == id);
            dataContext.Answers.Remove(answer);
            dataContext.SaveChanges();
        }

        public void ModifySession(TestSession testSession)
        {
            Entities.GroupSession session = null;
            if (testSession.Id == 0)
            {
                session = new GroupSession
                {
                    CourseId = testSession.CourseId,
                    CreatedDate = DateTime.Now,
                    EndDate = DateTime.Now.AddMinutes(testSession.TimeToLiveInMinutes ?? 0),
                    TestId = testSession.TestId
                };
                dataContext.GroupSessions.Add(session);
            }
            dataContext.SaveChanges();
            CreateSessionForEachStudent(session);
        }
        public IEnumerable<TestSession> GetSessions(int courseId)
        {
            var dbSessions = dataContext.GroupSessions.Include(e => e.Test).Where(e => e.CourseId == courseId).OrderByDescending(e => e.CreatedDate);
            return dbSessions.Select(e => Converter.ConvertToSession(e));
        }
        public void CreateSessionForEachStudent(GroupSession session)
        {
            int groupId = dataContext.Courses.FirstOrDefault(e => e.Id == session.CourseId).GroupId;
            var students = dataContext.Students.Where(e => e.GroupId == groupId);
            var personalSessions = students.Select(e =>
                new Session
                {
                    StudentId = e.Id,
                    GroupSessionId = session.Id,
                    StartDate = session.CreatedDate,
                    EndDate = session.EndDate
                });
            dataContext.Sessions.AddRange(personalSessions);
            dataContext.SaveChanges();
        }

        public IEnumerable<WebModels.SessionTestItem> GetActiveTestForStudent(int studentId)
        {
            var groupsIds = Specification.GetStudentGroupIds(studentId);
            var coursesIds = dataContext.Courses.Where(e => groupsIds.Contains(e.GroupId)).Select(e => e.Id);
            var groupSessionsIds = dataContext.GroupSessions
                .Where(e => coursesIds.Contains(e.CourseId))
                .Select(e => e.Id)
                .ToList();
            var personalSessions = dataContext.Sessions
                .Where(e =>
                    e.EndDate >= DateTime.Now
                    && groupSessionsIds.Contains(e.GroupSessionId ?? 0)
                    && e.StudentId == studentId
                    && e.Mark == null)
                    .ToList();

            return personalSessions.Select(e => Converter.ConvertToTestSessionItem(e));
        }
        public void StartSession(int sessionId)
        {
            var session = dataContext.Sessions.FirstOrDefault(e => e.Id == sessionId && e.EndDate <= DateTime.Now && e.Mark == null);
            if (session == null)
                throw new AppNotFoundException("Session for start not found");
            var test = dataContext.Tests.FirstOrDefault(e => e.Id == session.GroupSession.TestId);

            var questionsIds = PrepareRandomQuestionsIds(test);

            PreparedQuestions preparedQuestions = new PreparedQuestions
            {
                QuestionsIds = questionsIds.ConvertToString(),
                SessionId = sessionId
            };

            dataContext.PreparedQuestions.Add(preparedQuestions);
            dataContext.SaveChanges();
        }
        private IEnumerable<int> PrepareRandomQuestionsIds(Entities.Test test)
        {
            int countOfQuestions = test.StandartNumberOfQuestions ?? Consts.StandartNumberOfQuestions;
            var questions = dataContext.Questions.Where(e => e.TestId == test.Id).Select(e => e.Id).ToList();
            
            Random rnd = new Random();
            questions = questions.OrderBy(x => rnd.Next()).Take(countOfQuestions).ToList();

            return questions;
        }
    }
}
