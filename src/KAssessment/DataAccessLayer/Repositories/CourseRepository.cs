﻿using DataAccessLayer.Interfaces;
using Microsoft.EntityFrameworkCore;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Repositories
{
    public class CourseRepository : ICourseRepository
    {
        private readonly DataContext dataContext;
        private readonly Converter Converter;
        public CourseRepository(DataContext dataContext, Converter convert)
        {
            this.dataContext = dataContext;
            this.Converter = convert;
        }
        public Course GetCourseInfo(int subjectId, int groupId, int teacherId)
        {
            var dbCourses = dataContext.Courses
                .Include(e => e.Teacher)
                .Include(e => e.Subject)
                .Include(e => e.GroupSessions)
                .Include(e => e.Group)
                .ThenInclude(e => e.Specialty)
                .FirstOrDefault(e => e.TeacherId == teacherId && e.SubjectId == subjectId && e.GroupId == groupId);

            return Converter.ConvertToCourse(dbCourses);
        }
    }
}
