﻿using BusinessLogic.Exceptions;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using Microsoft.EntityFrameworkCore;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Repositories
{
    public class StudentRepository: IStudentRepository
    {
        private readonly DataContext dataContext;
        protected readonly Specification Specification;
        private readonly Converter Converter;
        public StudentRepository(DataContext dataContext,
            Specification specification,
            Converter converter)
        {
            this.dataContext = dataContext;
            Specification = specification;
            this.Converter = converter;
        }

        public bool CheckGroup(string GroupCode)
        {
            var group = dataContext.Groups.FirstOrDefault(e => e.UUID == GroupCode);
            if (group == null)
                return false;
            return true;
        }

        public StudentInfoModel GetStudentByUsername(string username)
        {
            var user = dataContext.Users.FirstOrDefault(e=>e.UserName==username);
            var dbStudent = dataContext.Students.Include(s=>s.AppUser).FirstOrDefault(e => e.AppUserId == (user!=null?user.Id:0));
            if (dbStudent == null)
                throw new AppNotFoundException("Student not found");
            return Converter.ConvertToStudentInfo(dbStudent);
        }

        public void RegisterStudent(int UserId,string groupCode)
        {
            var user = dataContext.Users.FirstOrDefault(e => e.Id == UserId);
            if (user == null)
                throw new AppNotFoundException("User not found");

            var group = dataContext.Groups.FirstOrDefault(e => e.UUID == groupCode);
            if (group == null)
                throw new AppNotFoundException("Group not found");

            Student newStudent = new Student
            {
                AppUserId = UserId,
                GroupId = group.Id
            };

            dataContext.Students.Add(newStudent);
            dataContext.SaveChanges();
        }
    }
}
