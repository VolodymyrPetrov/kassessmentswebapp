﻿using BusinessLogic;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Models.Entities;
using Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class TeacherRepository : ITeacherRepository
    {
        private readonly DataContext dataContext;
        private readonly UserManager<AppUser> userManager;
        private readonly SignInManager<AppUser> signInManager;
        private readonly ContentCreater emailContentCreater;
        private readonly EmailProvider emailProvider;
        private readonly Converter Converter;
        private readonly ISubjectRepository subjectRepository;
        public TeacherRepository(DataContext dataContext,
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,
            IConfiguration configuration,
            ISubjectRepository subjectRepository,
            Converter converter)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.dataContext = dataContext;
            this.emailContentCreater = new ContentCreater();
            var emailSection = configuration.GetSection("EmailAccount");
            this.emailProvider = new EmailProvider(emailSection.GetSection("Email").Value, emailSection.GetSection("Password").Value);
            this.subjectRepository = subjectRepository;
            this.Converter = converter;
        }

        public TeacherInfoModel GetTeacher(int teacherId)
        {
            var teacher = getDbTeacher(teacherId);
            if (teacher == null)
            {
                throw new Exception("Teacher not found");
            }
            return Converter.ConvertTeacherToModel(teacher);
        }
        private Teacher getDbTeacher(int teacherId)
        {
            return dataContext.Teachers.Include(e => e.AppUser).FirstOrDefault(e => e.Id == teacherId);
        }
        public void AssignTeacherToGroup(int TeacherId, int SubjectId, int GroupId)
        {
            var teacher = dataContext.Teachers.FirstOrDefault(e => e.Id == TeacherId);
            if (teacher == null)
                throw new Exception("Teacher not found");

            var subject = dataContext.Subjects.FirstOrDefault(e => e.Id == SubjectId);
            if (subject == null)
                throw new Exception("Subject not found");

            var group = dataContext.Groups.FirstOrDefault(e => e.Id == GroupId);
            if (group == null)
                throw new Exception("Group not found");

            Entities.Course teacherSubject = new Entities.Course
            {
                SubjectId = SubjectId,
                TeacherId = TeacherId,
                GroupId = GroupId
            };

            dataContext.Courses.Add(teacherSubject);
            dataContext.SaveChanges();
        }
        public async Task ModifyTeacher(TeacherInfoModel teacherInfo)
        {
            if (teacherInfo.Id == 0)
            {
                teacherInfo.Id = await RegisterTeacher(new TeacherRegisterModel
                {
                    Email = teacherInfo.Email,
                    FirstName = teacherInfo.FirstName,
                    LastName = teacherInfo.LastName,
                    MiddleName = teacherInfo.MiddleName
                });
            }
            else
            {
                var dbTeacher = dataContext.Teachers.Include(e => e.AppUser).FirstOrDefault(e => e.Id == teacherInfo.Id);
                dbTeacher.AppUser.LastName = teacherInfo.LastName;
                dbTeacher.AppUser.MiddleName = teacherInfo.MiddleName;
                dbTeacher.AppUser.FirstName = teacherInfo.FirstName;
            }
            dataContext.SaveChanges();
        }
        private async Task<int> RegisterTeacher(TeacherRegisterModel registerModel)
        {
            var user = new AppUser
            {
                Email = registerModel.Email,
                UserName = registerModel.Email,
                FirstName = registerModel.FirstName,
                LastName = registerModel.LastName,
                MiddleName = registerModel.MiddleName
            };
            var result = await userManager.CreateAsync(user);
            if (result.Succeeded)
            {
                string teacherRole = Roles.Teacher.ToString();
                await userManager.AddToRoleAsync(user, teacherRole);
                var teacherEntity = new Teacher { AppUserId = user.Id };
                dataContext.Teachers.Add(teacherEntity);
                var emailIdentificator = new ConfirmRegister { UserId = user.Id };
                dataContext.ConfirmRegisters.Add(emailIdentificator);
                dataContext.SaveChanges();
                await SendRegistringEmail(emailIdentificator.UUID, user);
                return teacherEntity.Id;
            }
            else
            {
                throw new Exception("Щось пішло не так");
            }
        }
        private async Task SendRegistringEmail(Guid UUID, AppUser user)
        {
            var emailBody = emailContentCreater.GetRegisterEmailContent(UUID, user);
            await emailProvider.SendEmailForFinishingRegistring(user.Email, emailBody);
        }
        public IEnumerable<TeacherInfoModel> GetTeachers()
        {
            return dataContext.Teachers.Include(e => e.AppUser).Select(e => Converter.ConvertTeacherToModel(e));
        }

        public void DeactivateTeacher(int teacherId)
        {
            var teacher = getDbTeacher(teacherId);
            teacher.AppUser.IsDeleted = true;
            dataContext.SaveChanges();
        }
        public void ActivateTeacher(int teacherId)
        {
            var teacher = getDbTeacher(teacherId);
            teacher.AppUser.IsDeleted = false;
            dataContext.SaveChanges();
        }

        public IEnumerable<Models.Entities.Course> GetTeacherCourses(string userName)
        {
            var teacher = getTeacherByUserName(userName);
            var courses = dataContext.Courses
                .Include(e=>e.Teacher)
                .Include(e=>e.Subject)
                .Include(e=>e.Group)
                .ThenInclude(e=>e.Specialty)
                .Where(e => e.TeacherId == teacher.Id)
                .ToList()
                .Select(e => Converter.ConvertToCourse(e))
                .OrderByDescending(e => e.Group.YearOfEnd);

            return courses;
        }
        
        private Teacher getTeacherByUserName(string userName)
        {
            var user = dataContext.Users.FirstOrDefault(e => e.UserName == userName);
            var teacher = dataContext.Teachers.Include(e => e.AppUser).FirstOrDefault(e => e.AppUserId == user.Id);
            return teacher;
        }

        public void CreateCourse(CreateCourseDTO model)
        {
            Entities.Course groupSubjectConfig = new Entities.Course
            {
                CreatedDate = DateTime.UtcNow,
                GroupId = model.GroupId,
                SubjectId = model.SubjectId,
                TeacherId = model.TeacherId
            };

            dataContext.Courses.Add(groupSubjectConfig);
            dataContext.SaveChanges();
        }

        public TeacherInfoModel GetTeacherByUserName(string UserName)
        {
            var dbTeacher = getTeacherByUserName(UserName);
            return Converter.ConvertTeacherToModel(dbTeacher);
        }

        public IEnumerable<TestsListItemDTO> GetTeacherTests(string userName, int? subjectId = null)
        {
            var dbTeacher = getTeacherByUserName(userName);
            var dbgrouppedTests = dataContext.Tests.Where(e => e.TeacherId == dbTeacher.Id && (subjectId==null || e.SubjectId==subjectId)).GroupBy(e => e.SubjectId).ToList();
            var tests = dbgrouppedTests.Select(e=> new TestsListItemDTO
            {
                TeacherId = dbTeacher.Id,
                Tests = e.Select(i => Converter.ConvertToTestTiny(i)),
                Subject = subjectRepository.GetSubject(e.Key)
            });
            return tests;
        }
    }
}
