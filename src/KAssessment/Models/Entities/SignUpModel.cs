﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.Entities
{
    public class StudentSignUpModel
    {
        [Required]
        [Display(Name = "Код групи")]
        public string GroupCode { get; set; }
        [Required]
        [Display(Name = "Прізвище")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Ім'я")]
        public string FirstName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }
        [Required]
        [Display(Name = "Підтвердіть пароль")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Паролі не співпадають!")]
        public string ConfirmPassword { get; set; }
    }
}
