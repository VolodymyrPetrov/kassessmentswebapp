﻿namespace Models.Entities
{
    public class Specialty
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PartialName { get; set; }
        public byte YearsOfStuding { get; set; }
    }
}
