﻿namespace Models.Entities
{
    public class StudentInfoModel
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
    }
}
