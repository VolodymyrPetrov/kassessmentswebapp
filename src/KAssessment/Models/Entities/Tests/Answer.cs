﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Entities
{
    public class Answer
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsRight { get; set; }
        public int QuestionId { get; set; }
    }
}
