﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Entities
{
    public class TestsListItemDTO
    {
        public Subject Subject { get; set; }
        public IEnumerable<Test> Tests{get;set;}
        public int TeacherId { get; set; }
    }
}
