﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Entities
{
    public class SessionTestItem
    {
        public int PersonSessionId { get; set; }
        public int? GroupSessionId { get; set; }
        public Test Test { get; set; }
        public string SubjectName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Mark { get; set; }
        public int StudentId { get; set; }
    }
}
