﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Entities
{
    public class Question
    {
        public int Id { get; set; }
        public string QuestionText { get; set; }
        public IEnumerable<Answer> Answers { get; set; } = new List<Answer>();
        public int TestId { get; set; }
    }
}
