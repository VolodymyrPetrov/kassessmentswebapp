﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Entities
{
    public class TestSession
    {
        public int Id { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int TestId { get; set; }
        public Test Test { get; set; }
        public int CourseId { get; set; }
        public int? TimeToLiveInMinutes { get; set; }
    }
}
