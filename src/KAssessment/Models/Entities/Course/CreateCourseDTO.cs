﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Entities
{
    public class CreateCourseDTO
    {
        public int TeacherId { get; set; }
        public int GroupId { get; set; }
        public int SubjectId { get; set; }
    }
}
