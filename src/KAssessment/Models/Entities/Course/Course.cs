﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Entities
{
    public class Course
    {
        public int Id { get; set; }
        public Subject Subject { get; set; }
        public Group Group { get; set; }
        public TeacherInfoModel Teacher { get; set; }
        public DateTime CreatedDate { get; set; }
        public IEnumerable<TestSession> TestSessions { get; set; }
    }
}
