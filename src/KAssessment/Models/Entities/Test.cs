﻿using System.Collections.Generic;

namespace Models.Entities
{
    public class Test
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int TeacherId { get; set; }
        public int SubjectId { get; set; }
        public string Name { get; set; }
        public int? NumberOfQuestions { get; set; }
        public IEnumerable<Question> Questions { get; set; }
    }
}
