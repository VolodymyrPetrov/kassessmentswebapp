﻿namespace Models.Entities
{
    public class Group
    {
        public int Id { get; set; }
        public int SpecialtyId { get; set; }
        public Specialty Specialty { get; set; }
        public short YearOfEntry { get; set; }
        public short YearOfEnd { get; set; }
        public byte Number { get; set; }
        public string Name { get; set; }
        public string UUID { get; set; }
    }
}