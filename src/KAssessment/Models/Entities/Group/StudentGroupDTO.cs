﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Entities
{
    public class StudentGroupDTO
    {
        public string GroupName { get; set; }
        public IEnumerable<StudentInfoModel> Students { get; set; }
    }
}
