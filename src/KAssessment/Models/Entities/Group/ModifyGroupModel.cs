﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Entities
{
    public class ModifyGroupModel
    {
        public Group Group { get; set; }
        public IEnumerable<Specialty> Specialties { get; set; }
    }
}
