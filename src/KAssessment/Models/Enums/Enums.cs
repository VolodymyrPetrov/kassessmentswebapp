﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Enums
{
    public enum Roles
    {
        Admin=1,
        Teacher,
        Student
    }
}
