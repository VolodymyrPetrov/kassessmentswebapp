﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
namespace BusinessLogic
{
    public class EmailProvider
    {
        private string fromEmail;
        private string password;
        public EmailProvider(string email,string password)
        {
            this.fromEmail = email;
            this.password = password;
        }
        public async Task SendEmailForFinishingRegistring(string toEmail, string body)
        {
            MailAddress from = new MailAddress(fromEmail, "KAssessmnet");
            MailAddress to = new MailAddress(toEmail);
            MailMessage m = new MailMessage(from, to);
            m.Subject = "Завершення реєстрації";
            m.Body = body;
            m.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = new NetworkCredential(fromEmail, password);
            smtp.EnableSsl = true;
            await smtp.SendMailAsync(m);
        }
    }
}
