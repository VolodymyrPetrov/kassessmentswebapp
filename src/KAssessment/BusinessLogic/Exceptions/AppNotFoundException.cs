﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Exceptions
{
    public class AppNotFoundException : AppExceptionBase
    {
        public AppNotFoundException(string message) : base(message)
        {
        }
    }
}
