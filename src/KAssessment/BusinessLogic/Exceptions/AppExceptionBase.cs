﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Exceptions
{
    public class AppExceptionBase:Exception
    {
        public AppExceptionBase(string message) : base(message) { }
    }
}
