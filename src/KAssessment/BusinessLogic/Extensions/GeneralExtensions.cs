﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Extensions
{
    public static class GeneralExtensions
    {
        public static string ConvertToString(this IEnumerable<int> values, char separator=',')
        {
            return string.Join(separator,values);
        }
        public static IEnumerable<int> ConvertToList(this string text, char separator = ',')
        {
            return text.Split(separator).Select(e => Convert.ToInt32(e));
        }
    }
}
