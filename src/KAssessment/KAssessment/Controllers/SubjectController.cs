﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Models.Entities;

namespace KAssessment.Controllers
{
    public class SubjectController : Controller
    {
        private readonly ISubjectRepository subjectRepository;
        public SubjectController(ISubjectRepository subjectRepository)
        {
            this.subjectRepository = subjectRepository;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View(subjectRepository.GetSubjects());
        }
        [HttpPost]
        public IActionResult Modify(Subject subject)
        {
            subjectRepository.ModifySubject(subject);
            return Redirect("Index");
        }
    }
}