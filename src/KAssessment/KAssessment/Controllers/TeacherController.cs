﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer;
using DataAccessLayer.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.Entities;

namespace KAssessment.Controllers
{
    [Authorize(Roles = "Teacher")]
    public class TeacherController : Controller
    {
        private Repository repository;
        public TeacherController(Repository repository)
        {
            this.repository = repository;
        }
        [HttpGet]
        public IActionResult Courses()
        {
            var teacherCourses = repository.Teachers.GetTeacherCourses(User.Identity.Name);
            return View(teacherCourses);
        }
        [HttpGet]
        public IActionResult Tests()
        {
            var teacherTests = repository.Teachers.GetTeacherTests(User.Identity.Name);
            return View(teacherTests);
        }
        [HttpGet]
        public IActionResult ModifyTest(int testId = 0, int subjectId = 0)
        {
            ViewBag.Subjects = repository.Subjects.GetSubjects();
            ViewBag.SelectedSubject = subjectId;
            var test = testId == 0 ? new Test() : repository.Tests.GetTest(testId);
            return View(test);
        }
        [HttpPost]
        public IActionResult ModifyTest(Test test)
        {
            test.TeacherId = repository.Teachers.GetTeacherByUserName(User.Identity.Name).Id;
            repository.Tests.ModifyTest(test);
            return RedirectToAction("Tests");
        }
        [HttpGet]
        public IActionResult TestQuestions([FromRoute]int id)
        {
            var testInfo = repository.Tests.GetTestWithQuestions(id);
            return View(testInfo);
        }
        [HttpPost]
        public IActionResult ModifyQuestion(Question question)
        {
            repository.Tests.ModifyTestQuestion(question);
            return RedirectToAction("TestQuestions", new { id = question.TestId });
        }
        [HttpDelete]
        public IActionResult DeleteTest(int testId)
        {
            repository.Tests.DeleteTest(testId);
            return RedirectToAction("Tests");
        }
        [HttpPost]
        public IActionResult ModifyAnswer(Answer answer)
        {
            var result = repository.Tests.ModifyQuestionAnswer(answer);
            var question = repository.Tests.GetQuestion(result.QuestionId);
            return PartialView("_QuestionAnswers", question);
        }
        [HttpPost]
        public IActionResult DeleteAnswer(int id)
        {
            var answer = repository.Tests.GetAnswer(id);
            repository.Tests.DeleteAnswer(id);
            var question = repository.Tests.GetQuestion(answer.QuestionId);
            return PartialView("_QuestionAnswers", question);
        }
        [HttpGet]
        public IActionResult Course(int subjectId,int teacherId,int groupId)
        {
            var model = repository.Course.GetCourseInfo(subjectId:subjectId,teacherId:teacherId,groupId:groupId);
            ViewBag.AvailableTests = repository.Teachers.GetTeacherTests(User.Identity.Name,subjectId).FirstOrDefault()?.Tests;
            return View(model);
        }
        [HttpPost]
        public IActionResult Session(TestSession testSession)
        {
            repository.Tests.ModifySession(testSession);
            return PartialView("_TestSessions",
                repository.Tests.GetSessions(testSession.CourseId));
        }
    }
}