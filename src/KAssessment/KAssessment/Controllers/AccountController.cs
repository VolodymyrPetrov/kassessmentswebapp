﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer.Entities;
using Models.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models.Entities;
using DataAccessLayer.Interfaces;

namespace KAssessment.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<AppUser> userManager;
        private readonly SignInManager<AppUser> signInManager;
        private readonly IUserRepository userRepository;
        private readonly IStudentRepository studentRepository;
        public AccountController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, IUserRepository userRepository, IStudentRepository studentRepository)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.userRepository = userRepository;
            this.studentRepository = studentRepository;
        }
        [HttpGet]
        public IActionResult StudentSignUp()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> StudentSignUp([FromForm]StudentSignUpModel signUp)
        {
            if (!ModelState.IsValid)
            {
                return View(signUp);
            }
            if (!studentRepository.CheckGroup(signUp.GroupCode))
            {
                ModelState.AddModelError("GroupCode", "Не правильний код групи");
            }

            var user = new AppUser
            {
                Email = signUp.Email,
                UserName = signUp.Email,
                LastName = signUp.LastName,
                FirstName = signUp.FirstName
            };
            var result = await userManager.CreateAsync(user, signUp.Password);
            if (result.Succeeded)
            {
                string role = Roles.Student.ToString();
                await userManager.AddToRoleAsync(user, role);
                var signInUser = await userManager.FindByEmailAsync(signUp.Email);
                studentRepository.RegisterStudent(signInUser.Id, signUp.GroupCode);
                await signInManager.PasswordSignInAsync(signInUser, signUp.Password, false, false);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
                return View(signUp);
            }
        }
        //[HttpGet]
        //public IActionResult AdminSignUp()
        //{
        //    return View();
        //}
        //[HttpPost]
        //public async Task<IActionResult> AdminSignUp([FromForm]SignUpModel signUp)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(signUp);
        //    }
        //    var user = new AppUser { Email = signUp.Email, UserName = signUp.Email };
        //    var result = await userManager.CreateAsync(user, signUp.Password);
        //    if (result.Succeeded)
        //    {
        //        string adminRole = Roles.Admin.ToString();
        //        await userManager.AddToRoleAsync(user,adminRole);
        //        var signInUser = await userManager.FindByEmailAsync(signUp.Email);
        //        await signInManager.PasswordSignInAsync(signInUser, signUp.Password, false, false);
        //        return RedirectToAction("Index", "Home");
        //    }
        //    else
        //    {
        //        foreach (var error in result.Errors)
        //        {
        //            ModelState.AddModelError(string.Empty, error.Description);
        //        }
        //        return View(signUp);
        //    }
        //}
        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            return View(new LoginUserModel { ReturnUrl = returnUrl });
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> Login([FromForm]LoginUserModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await userManager.FindByEmailAsync(model.Email);
            if (user == null || !(await userManager.CheckPasswordAsync(user, model.Password)))
            {
                ModelState.AddModelError("Password", "Неправильний логін і (або) пароль");
                return View(model);
            }
            if (user.IsDeleted)
            {
                ModelState.AddModelError("Email", "Ваш обліковий запис не активний");
                return View(model);
            }
            var result = await signInManager.PasswordSignInAsync(user.UserName, model.Password, model.RememberMe, false);
            if (result.Succeeded)
            {
                if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                {
                    return Redirect(model.ReturnUrl);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                ModelState.AddModelError("Password", "Щось пішло не так");
                return View(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Login");
        }
        [HttpGet]
        [ActionName("FinishRegistring")]
        public IActionResult FinishRegistring([FromRoute]Guid id)
        {
            var model = userRepository.GetModelForFinishingRegistrigFromEmailLink(id);
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> FinishRegistring(FinishRegistringFromLinkModel signUp)
        {
            await userRepository.FinishRegistrigFromEmailLink(signUp.uuid, signUp.Password);
            return RedirectToAction("Login");
        }
    }
}