﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Models.Entities;

namespace KAssessment.Controllers
{
    public class CourseController : Controller
    {
        private readonly ITeacherRepository teacherRepository;
        private readonly IOrganizationRepository organizationRepository;
        private readonly ISubjectRepository subjectRepository;
        public CourseController(ITeacherRepository teacherRepository, IOrganizationRepository organizationRepository, ISubjectRepository subjectRepository)
        {
            this.teacherRepository = teacherRepository;
            this.organizationRepository = organizationRepository;
            this.subjectRepository = subjectRepository;
        }
        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Groups = organizationRepository.GetAvailableGroups();
            ViewBag.Subjects = subjectRepository.GetSubjects();
            return View(new CreateCourseDTO());
        }
        [HttpPost]
        public IActionResult Create(CreateCourseDTO createCourse)
        {
            var teacherUserName = User.Identity.Name;
            createCourse.TeacherId = teacherRepository.GetTeacherByUserName(teacherUserName).Id;
            teacherRepository.CreateCourse(createCourse);
            return RedirectToAction("Courses","Teacher");
        }
    }
}