﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Models.Entities;

namespace KAssessment.Controllers
{
    public class GroupController : Controller
    {
        private readonly IOrganizationRepository organizationRepository;
        public GroupController(IOrganizationRepository organizationRepository)
        {
            this.organizationRepository = organizationRepository;
        }
        [HttpGet]
        public IActionResult Index(int specialtyId = 0)
        {
            return View(organizationRepository.GetGroups(specialtyId));
        }
        [HttpGet]
        public IActionResult Modify([FromRoute]int id = 0)
        {
            ModifyGroupModel model = new ModifyGroupModel();
            model.Specialties = organizationRepository.GetSpecialties();

            if (id == 0)
            {
                model.Group = new Group();
            }
            else
            {
                model.Group = organizationRepository.GetGroup(id);
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult Modify(Group group)
        {
            organizationRepository.EditGroup(group);
            return Redirect("Index");
        }
        [HttpDelete]
        public IActionResult DeactivateGroup(int groupId)
        {
            organizationRepository.DeleteGroup(groupId);
            return Redirect("Index");
        }  
    }
}