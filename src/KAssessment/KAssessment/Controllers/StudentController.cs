﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer;
using Microsoft.AspNetCore.Mvc;

namespace KAssessment.Controllers
{
    public class StudentController : Controller
    {
        private readonly Repository repository;
        public StudentController(Repository repository)
        {
            this.repository = repository;
        }
        [HttpGet]
        public IActionResult Group([FromRoute]int id)
        {
            return View(repository.Organization.GetGroupStudents(id));
        }
        [HttpGet]
        public IActionResult OpenedTests()
        {
            var student = repository.Students.GetStudentByUsername(User.Identity.Name);
            return View(repository.Tests.GetActiveTestForStudent(student.StudentId));
        }
    }
}