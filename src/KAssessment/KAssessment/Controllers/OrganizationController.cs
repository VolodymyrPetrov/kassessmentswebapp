﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Models.Entities;

namespace KAssessment.Controllers
{
    public class OrganizationController : Controller
    {
        private readonly IOrganizationRepository organizationRepository;
        private readonly ITeacherRepository teacherRepository;
        public OrganizationController(ITeacherRepository teacherRepository, IOrganizationRepository organizationRepository)
        {
            this.teacherRepository = teacherRepository;
            this.organizationRepository = organizationRepository;
        }
        [HttpGet]
        public IActionResult Teacher([FromQuery]int teacherId)
        {
            var teacher = teacherRepository.GetTeacher(teacherId);
            return View(teacher);
        }
        [HttpGet]
        public IActionResult Teachers()
        {
            var teachers = teacherRepository.GetTeachers();
            return View(teachers);
        }
        [HttpGet]
        [ActionName("ModifyTeacher")]
        public IActionResult ModifyTeacher([FromRoute]int id = 0)
        {
            var teacher = id == 0 ? new TeacherInfoModel() : teacherRepository.GetTeacher(id);
            return View(teacher);
        }
        [HttpPost]
        public async Task<IActionResult> ModifyTeacher(TeacherInfoModel teacherInfo)
        {
            await teacherRepository.ModifyTeacher(teacherInfo);
            return RedirectToAction("Teachers");
        }
        public IActionResult DeactivateTeacher([FromRoute]int id = 0)
        {
            teacherRepository.DeactivateTeacher(id);
            return RedirectToAction("Teachers");
        }
        public IActionResult ActivateTeacher([FromRoute]int id = 0)
        {
            teacherRepository.ActivateTeacher(id);
            return RedirectToAction("Teachers");
        }
        [HttpGet]
        public IActionResult Specialties()
        {
            return View(organizationRepository.GetSpecialties());
        }
        [HttpGet]
        public async Task<IActionResult> ModifySpecialty([FromRoute]int id = 0)
        {
            return View(await organizationRepository.GetSpecialty(id));
        }
        [HttpPost]
        public async Task<IActionResult> ModifySpecialty(Specialty specialty)
        {
            await organizationRepository.EditSpecialty(specialty);
            return RedirectToAction("Specialties");
        }
        [HttpDelete]
        public IActionResult DeleteSpecialty(int specialtyId)
        {
            organizationRepository.DeleteSpecialty(specialtyId);
            return RedirectToAction("Specialties");
        }
    }
}