﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer;
using Microsoft.AspNetCore.Mvc;

namespace KAssessment.Controllers
{
    public class SessionController : Controller
    {
        private readonly Repository _repository;
        public SessionController(Repository repository)
        {
            _repository = repository;
        }
        public IActionResult Start([FromRoute]int id)
        {
            //_repository.Tests.StartTest(id);
            return View();
        }
        public IActionResult Result([FromRoute]int id)
        {
            return View();
        }
    }
}